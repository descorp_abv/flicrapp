//
//  FlickrImagesVC+SearchDelegate.swift
//  FlickrApp
//
//  Created by Vladimir Abramichev on 07/11/16.
//  Copyright © 2016 Vladimir Abramichev. All rights reserved.
//

import Foundation
import UIKit

extension FlickrImagelistViewController : UISearchBarDelegate{
    
    func addSearchBar(){
        if self.searchBar == nil {
            let searchBarBoundsY = (self.navigationController?.navigationBar.frame.size.height)! + UIApplication.shared.statusBarFrame.size.height
            
            self.searchBar = UISearchBar(frame: CGRect(x: 0, y: searchBarBoundsY, width: UIScreen.main.bounds.size.width, height: 44))
            self.searchBar!.searchBarStyle       = .minimal
            self.searchBar!.tintColor            = UIColor.white
            self.searchBar!.barTintColor         = UIColor.white
            self.searchBar!.delegate             = self;
            self.searchBar!.placeholder          = "search here";
        }
        
        if !self.searchBar!.isDescendant(of: self.view){
            self.view .addSubview(self.searchBar!)
        }
    }
    
    func updateSearchBar(){
        let searchBarBoundsY = (self.navigationController?.navigationBar.frame.size.height)! + UIApplication.shared.statusBarFrame.size.height
        var screen = UIScreen.main.bounds.size
        
        self.searchBar?.frame = CGRect(x: 0, y: searchBarBoundsY, width: screen.width, height: 44)
    }
    
    // MARK: Search
    func filterContentForSearchText(searchText:String){
        if(searchText.contains("#")){
            let tags = searchText.components(separatedBy: " #")
            self.search = TagSearch(tags: tags)
            
        } else
        {
            self.search = TextSearch(textSearch: searchText)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // user did type something, check our datasource for text that looks the same
        if searchText.characters.count > 0 {
            // search and reload data source
            self.searchBarActive    = true
            self.filterContentForSearchText(searchText: searchText)
        }else{
            // if text lenght == 0
            // we will consider the searchbar is not active
            self.searchBarActive = false
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self .cancelSearching()
        self.collectionView?.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBarActive = true
        self.DataContext.removeAll()
        self.search.getNextPage(compleated: self.AppendDataContext)
        self.view.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        // we used here to set self.searchBarActive = YES
        // but we'll not do that any more... it made problems
        // it's better to set self.searchBarActive = YES when user typed something
        self.searchBar!.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        // this method is being called when search btn in the keyboard tapped
        // we set searchBarActive = NO
        // but no need to reloadCollectionView
        self.searchBarActive = false
        self.searchBar!.setShowsCancelButton(false, animated: false)
    }
    
    func cancelSearching(){
        self.searchBarActive = false
        self.searchBar!.resignFirstResponder()
        self.searchBar!.text = ""
    }
}
