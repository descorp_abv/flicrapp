//
//  FlickrImagelistViewController.swift
//  FlickrApp
//
//  Created by Vladimir Abramichev on 03/11/16.
//  Copyright © 2016 Vladimir Abramichev. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

enum LayoutType: Int
{
    case Grid = 0
    case List = 1
}

class FlickrImagelistViewController : UICollectionViewController {
    
    let cellId = "FlickrPhotoItem"

    var DataContext : Array<FlickrPhoto> = Array<FlickrPhoto>()
    
    var search : SearchStrategy = TopInteresting()
    
    var searchBarActive:Bool = false
    var searchBar:UISearchBar?
    
    var currentLayout = LayoutType.List
    var imageDimmentions : Array<CGSize> = Array<CGSize>()
    
    //MARK: lifecycle
    override func viewDidLoad() {
        search.getNextPage(compleated: self.AppendDataContext)
        
        self.collectionView?.contentInset = UIEdgeInsetsMake(44, 0, 0, 0);
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(changeLayout))
        self.addSearchBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.collectionView?.collectionViewLayout.invalidateLayout()
        
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChange), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    func orientationChange() {
        self.collectionView?.collectionViewLayout.invalidateLayout()        
        self.updateSearchBar()
    }
    
    //MARK: data source
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataContext.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let visibleCount = self.collectionView?.visibleCells.count ?? 0
        let overallCount = DataContext.count
        if( visibleCount < overallCount && indexPath.item >= DataContext.count - visibleCount)
        {
            search.getNextPage(compleated: self.AppendDataContext)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView!.dequeueReusableCell(withReuseIdentifier: self.cellId, for: indexPath) as! FlickrCell
        cell.label.text = DataContext[indexPath.item].Title
        
        cell.image.sd_setImage(with: DataContext[indexPath.item].UrlThumb)        
        return cell
    }
    
    //MARK: delegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
        let newViewController = sb.instantiateViewController(withIdentifier: "photoViewControllerId")
        (newViewController as! PhotoViewController).context = DataContext[indexPath.item]
        self.navigationController!.pushViewController(newViewController, animated: true)
    }
    
    // MARK: Observers

    
    func addObservers(){
        let context = UnsafeMutablePointer<UInt8>(bitPattern: 1)
        self.collectionView?.addObserver(self, forKeyPath: "contentOffset", options: [.new,.old], context: context)
    }
    
    func removeObservers(){
        self.collectionView?.removeObserver(self, forKeyPath: "contentOffset")
    }
    
    // MARK: private methods
    
    func changeLayout() {
        if(currentLayout == .Grid) {
            self.currentLayout = .List
        } else{
            self.currentLayout = .Grid
        }
        
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    func AppendDataContext(photo : FlickrPhoto) {
        self.DataContext.append(photo)
        self.collectionView?.reloadData()
    }
    
    func AppendDataContext(photos : Array<FlickrPhoto>) {
        self.DataContext.append(contentsOf: photos)
        self.collectionView?.reloadData()
    }
}
