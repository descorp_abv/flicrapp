//
//  PhotoViewerController.swift
//  FlickrApp
//
//  Created by Vladimir Abramichev on 04/11/16.
//  Copyright © 2016 Vladimir Abramichev. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage


class PhotoViewController : UIViewController, UIGestureRecognizerDelegate {
    
    var context : FlickrPhoto?
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTrailingConstraint: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        self.title = self.context?.Title
        
        self.imageView.sd_setImage(with: self.context!.UrlOriginal)
        self.updateMinZoomScaleForSize(size: view.bounds.size)
    }
    
    private func updateMinZoomScaleForSize(size: CGSize) {
        let widthScale = size.width / imageView.bounds.width
        let heightScale = size.height / imageView.bounds.height
        let minScale = min(widthScale, heightScale)
        
        scrollView.minimumZoomScale = minScale
        scrollView.zoomScale = minScale
    }
}
