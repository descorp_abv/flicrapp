//
//  FlickrCell.swift
//  FlickrApp
//
//  Created by Vladimir Abramichev on 03/11/16.
//  Copyright © 2016 Vladimir Abramichev. All rights reserved.
//

import Foundation
import UIKit

class FlickrCell : UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var label: UILabel!
    
    
}
