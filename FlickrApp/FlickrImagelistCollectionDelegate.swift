//
//  FlickrImagelistVC+Delegate.swift
//  FlickrApp
//
//  Created by Vladimir Abramichev on 07/11/16.
//  Copyright © 2016 Vladimir Abramichev. All rights reserved.
//

import Foundation
import UIKit

extension FlickrImagelistViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var screen = UIScreen.main.bounds.size

        var itemSize : CGSize
        if self.currentLayout == LayoutType.Grid {
            var width = (screen.width - 15) / 3
            itemSize = (indexPath.item%3 == 1) ? CGSize(width: width, height: width) : CGSize(width: width + 1, height: width + 1)
        }
        else {
            var currentImage = DataContext[indexPath.item].Size
            var height = screen.width * currentImage.height / currentImage.width
            itemSize = CGSize(width: screen.width, height: height + 20)
        }
        
        return itemSize
    }
}
