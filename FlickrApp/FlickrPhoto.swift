//
//  FlickrPhoto.swift
//  FlickrApp
//
//  Created by Vladimir Abramichev on 04/11/16.
//  Copyright © 2016 Vladimir Abramichev. All rights reserved.
//

import Foundation
import UIKit


public class FlickrPhoto
{
    let UrlThumb : URL
    
    let UrlOriginal : URL
    
    let Title : String
    
    let Size : CGSize
    
    init(url : URL, thumb : URL, title : String, size : CGSize) {
        self.UrlOriginal = url
        self.UrlThumb = thumb
        self.Title = title
        self.Size = size
    }
}
