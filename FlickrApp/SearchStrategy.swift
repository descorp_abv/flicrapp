//
//  SearchStrategy.swift
//  FlickrApp
//
//  Created by Vladimir Abramichev on 05/11/16.
//  Copyright © 2016 Vladimir Abramichev. All rights reserved.
//

import Foundation

protocol SearchStrategy
{
    func getNextPage(compleated: @escaping (Array<FlickrPhoto>) -> Void)
}

class TopInteresting : SearchStrategy
{
    var currentPage : Int = 0;
    
    internal func getNextPage(compleated : @escaping (Array<FlickrPhoto>) -> Void) {
        currentPage += 1
        FlickrService.showTodaysInteresting(page: currentPage, compleated: compleated)
    }
}

class TextSearch : SearchStrategy
{
    var currentPage : Int = 0;
    var searchContext : String
    
    init(textSearch : String) {
        self.searchContext = textSearch
    }
    
    internal func getNextPage(compleated : @escaping (Array<FlickrPhoto>) -> Void) {
        currentPage += 1
        return FlickrService.searchPhoto(search: self.searchContext, page: self.currentPage, compleated: compleated)
    }
}

class TagSearch : SearchStrategy
{
    var currentPage : Int = 0;
    var searchContext : [String]
    
    init(tags : [String]) {
        self.searchContext = tags
    }
    
    internal func getNextPage(compleated : @escaping (Array<FlickrPhoto>) -> Void) {
        currentPage += 1
        return FlickrService.searchPhoto(tags: self.searchContext, page: self.currentPage, compleated: compleated)
    }
}
