//
//  FlickrService.swift
//  FlickrApp
//
//  Created by Vladimir Abramichev on 04/11/16.
//  Copyright © 2016 Vladimir Abramichev. All rights reserved.
//

import Foundation
import FlickrKit

class FlickrService{
    
    static let pageSize = "15"
    static let extras = "o_dims, url_l, url_z, date_upload"
    
    static func showTodaysInteresting(page : Int, compleated :  @escaping (Array<FlickrPhoto>) -> Void) {
        let flickrInteresting = FKFlickrInterestingnessGetList()
        flickrInteresting.per_page = pageSize
        flickrInteresting.page = String(page)
        flickrInteresting.extras = extras
        
        self.performFlickrOperation(method: flickrInteresting, compleated: compleated)
    }
    
    static func searchPhoto(search : String, page : Int, compleated : @escaping (Array<FlickrPhoto>) -> Void) {
        let searchText = FKFlickrPhotosSearch()
        searchText.text = search
        searchText.per_page = pageSize
        searchText.page = String(page)
        searchText.extras = extras
        
        self.performFlickrOperation(method: searchText, compleated: compleated)
    }
    
    static func searchPhoto(tags : [String], page : Int, compleated :  @escaping (Array<FlickrPhoto>) -> Void) {
        let searchTags = FKFlickrPhotosSearch()
        searchTags.tags = tags.filter({ (x : String) -> Bool in x == tags.first}).reduce(tags.first) { $0 + ", " + $1}
        searchTags.per_page = pageSize
        searchTags.page = String(page)
        searchTags.extras = extras
        
        self.performFlickrOperation(method: searchTags, compleated: compleated)
    }
    
    private static func performFlickrOperation(method : FKFlickrAPIMethod, compleated:  @escaping (Array<FlickrPhoto>) -> Void) {
        var result = Array<FlickrPhoto>()
        FlickrKit.shared().call(method) { (response, error) -> Void in
            
            DispatchQueue.main.async(execute: { () -> Void in
                if (response != nil) {
                    // Pull out the photo urls from the results

                    let topPhotos = response?["photos"] as! NSDictionary
                    let photoArray : Array = topPhotos["photo"] as! [[NSObject: AnyObject]]
                    for photoDictionary in photoArray {
                        do{
                            let photo = photoDictionary as! NSDictionary
                            
                            if(photo.count < 12) {
                                continue
                            }
                        
                            let thumb = URL(string: photo["url_z"] as! String)
                            let origin = URL(string: (photo.value(forKey: "url_l") ?? photo["url_z"]!) as! String)
                            let title = photo["title"] as? String
                            let width = ConvertToInt(value: photo.value(forKey: "width_l") ?? photo["width_z"]!) ?? 1024
                            let height = ConvertToInt(value: photo.value(forKey: "height_l") ?? photo["height_z"]!) ?? 768
                        
                            result.append(FlickrPhoto(url : origin!, thumb : thumb!, title : title!, size: CGSize(width: width, height: height)))
                        }
                        catch {
                        
                        }
                    }
                    
                    compleated(result)
                }
                else {
                    // Iterating over specific errors for each service
                    if let error = error as? NSError {
                        switch error.code {
                            case FKFlickrInterestingnessGetListError.serviceCurrentlyUnavailable.rawValue:
                                break;
                            default:
                                break;
                        }
    
                        let alert = UIAlertView(title: "Error", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                        compleated(result)
                    }
                }
            })
        }
    }
    
    private static func ConvertToInt(value : Any) -> Int?
    {
        if(value is String)
        {
            return Int(value as! String)!
        }
        
        if(value is NSNumber)
        {
            return Int(value as! NSNumber)
        }
        
        return nil;
    }
}
